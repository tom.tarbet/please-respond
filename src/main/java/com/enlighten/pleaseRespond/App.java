package com.enlighten.pleaseRespond;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class App {
    public static void main(String[] args) {
        int defaultTime = 60;
        if (args.length > 0) {
            defaultTime = Integer.parseInt(args[0]);
        }
        App.listenForEvents(defaultTime);
    }

    private static void listenForEvents(int secsToListen) {

        try {

            HttpClient httpClient = HttpClient.newHttpClient();

            final InputStream eventStream;
            final JsonParser parser;
            final long start = System.currentTimeMillis();
            long timeElapsed = 0;

            HttpResponse<InputStream> httpResponse = httpClient.send(
                    HttpRequest.newBuilder(new URI("https://stream.meetup.com/2/rsvps")).GET().build(),
                    BodyHandlers.ofInputStream());

            eventStream = httpResponse.body();

            JsonFactory jfactory = new JsonFactory();
            jfactory.setCodec(new ObjectMapper());
            parser = jfactory.createParser(eventStream);
            Integer eventCount = 0;
            Long furthestEventTime = 0L;
            JsonNode furthesEvent = null;
            Map<String, Integer> countryEvents = new HashMap<String, Integer>();

            while (parser.nextToken() != null && timeElapsed < secsToListen * 1000) {
                ObjectNode tree = parser.readValueAs(ObjectNode.class);
                eventCount++;
                Long eventTime = tree.get("event").get("time").asLong();
                if (eventTime > furthestEventTime) {
                    furthestEventTime = eventTime;
                    furthesEvent = tree.get("event");
                }

                String country = tree.path("group").path("group_country").asText();
                countryEvents.merge(country, 1, Integer::sum);

                long finish = System.currentTimeMillis();
                timeElapsed = finish - start;
            }
            
            LinkedHashMap<String, Integer> c = countryEvents.entrySet().stream()
                    .sorted(Map.Entry.<String, Integer>comparingByValue().reversed()).collect(Collectors
                            .toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));

            while (c.size() < 3) {
                c.put("", 0);
            }

            Iterator<Entry<String, Integer>> iter = c.entrySet().iterator();
            Entry<String, Integer> c1 = iter.next();
            Entry<String, Integer> c2 = iter.next();
            Entry<String, Integer> c3 = iter.next();

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            System.out.println(String.join(",", eventCount + "", simpleDateFormat.format(new Date(furthestEventTime)),
                    furthesEvent.path("event_url").asText(), c1.getKey(), c1.getValue().toString(), c2.getKey(),
                    c2.getValue().toString(), c3.getKey(), c3.getValue().toString()));

        } catch (IOException | InterruptedException | URISyntaxException e) {
            throw new RuntimeException("Unable to parse event stream to Json", e);
        }
    }
}
